package main

import (
	"sort"
	"strconv"
)

func main() {
}

//ReturnInt return integer value
func ReturnInt() int {
	return 1
}

//ReturnFloat return float32 value
func ReturnFloat() float32 {
	return 1.1
}

//ReturnIntArray return array of integer
func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

//ReturnIntSlice return slice of integer
func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

//IntSliceToString return string
func IntSliceToString(sl []int) string {
	var resultString = ""
	for _, value := range sl {
		resultString += strconv.Itoa(value)
	}
	return resultString
}

//MergeSlices merge slices
func MergeSlices(sl1 []float32, sl2 []int32) []int {
	slTemp := make([]int, len(sl1))
	for ind, value := range sl1 {
		slTemp[ind] = int(value)
	}

	for _, value := range sl2 {
		slTemp = append(slTemp, int(value))
	}

	return slTemp
}

//GetMapValuesSortedByKey return map
func GetMapValuesSortedByKey(map1 map[int]string) []string {
	sl1 := []string{""}

	var keys []int
	for k := range map1 {
		keys = append(keys, k)
	}

	sort.Ints(keys)

	for _, key := range keys {
		sl1 = append(sl1, map1[key])
	}
	return sl1[1:]
}
